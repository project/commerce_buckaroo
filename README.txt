Introduction
============

Buckaroo integration for the Drupal Commerce payment and checkout system.

Buckaroo offers three payment methods for use in a Drupal Commerce shop:

- Credit card
- PayPal
- iDeal

Installation
=============

1. Enable the module
2. Configure it at admin/config/services/buckaroo
3. Enable payment methods